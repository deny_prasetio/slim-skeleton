# Slim 3 Skeleton

This is a simple skeleton project for Slim 3 that includes Twig, Flash messages, Monolog & ezSQL.

Twig template using SASS and bourbon family. Also included sample of "simple CRUD".

## Requirements
* PHP 5.5.x or newer
* MySQL Server 5.x or newer

## Create your project:

    $ composer create-project -n -s dev sunaryohadi/slim3-skeleton my-app

### Database

* Create database and import crud.sql .

### Run it:

1. `$ cd my-app`
2. `$ php -S 0.0.0.0:8888 -t public public/index.php`
3. Browse to http://localhost:8888

## Key directories

* `app`: Application code
* `app/src`: All class files within the `App` namespace
* `app/templates`: Twig template files
* `cache/twig`: Twig's Autocreated cache files
* `log`: Log files
* `public`: Webserver root
* `vendor`: Composer dependencies
* `sass`: Sass files using bourbon.io

## Key files

* `public/index.php`: Entry point to application
* `app/settings.php`: Configuration
* `app/dependencies.php`: Services for Pimple
* `app/middleware.php`: Application middleware
* `app/routes.php`: All application routes are here
* `app/src/Action/HomeAction.php`: Action class for the home page
* `app/templates/main.twig`: Main base Twig Template
* `app/templates/home.twig`: Twig template file for the home page

## Credits
* https://github.com/akrabat/slim3-skeleton
* http://justinvincent.com/ezsql
* http://bourbon.io